package Main;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Model.MongoDbQueries;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import View.ViewMain;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javafx.application.Application.launch;
import javafx.event.EventHandler;
import javafx.stage.WindowEvent;



/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class MongoDBMain extends Application {
    private int windowHeight=400;
    private int windowWidth=640;
    private static MongoDatabase db;
    private static MongoClient client;
    
    @Override
    public void start(Stage primaryStage) {
        MongoDbQueries model = new MongoDbQueries(db);
        ViewMain view = new ViewMain(model,windowWidth, windowHeight);
        Scene scene = new Scene(view, windowWidth, windowHeight);
        primaryStage.setResizable(false);
        
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
        @Override
        public void handle(final WindowEvent event) {
            System.out.println("closing");
            client.close();
        }
    });
        
        primaryStage.setTitle("Review");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        client = new MongoClient();
        db = client.getDatabase("lab2");
        
        launch(args);
    }
}
