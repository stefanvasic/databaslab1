/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;
import Controller.Controller;
import Model.Album;
import Model.Questions;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;

/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class ViewMain extends BorderPane {
    private int windowHeight, windowWidth;
    private Menu fileMenu;
    private MenuItem exit;
    private MenuBar menuBar;
    private GridPane  addAlbumPopUp,searchAlbumPopUp;
    private Label albumGenre,albumYear,albumTitle,addAlbumScreenLabel,artistLabel,
            artistName,artistNationality, artistAge, searchByLabel;
    private Button addAlbumButton, rateAlbumButton, searchAlbumButton, doneButton,addArtistButton, searchButton;
    private Stage addAlbumStage;
    private TextField albumYearField, albumTitleField,artistNameField,artistNationalityField, artistAgeField
            , searchAlbumTextField, rankValueField, rankField;
    private ComboBox albumGenreComboBox, searchByComboBox;
    private Controller controller;
    private Questions model;
    private VBox buttonVBox;
    private TableView tableView;
   
    private AnchorPane anchorPane;
    
    public ViewMain(Questions model,int windowWidth, int windowHeight){
        this.model = model;
        this.controller = new Controller(this, model);
        this.windowWidth=windowWidth;
        this.windowHeight=windowHeight;
        initView();
    }
    
    public void initView(){
        createMenuBar();
        createButtons();
    }
    
    public void createMenuBar(){
        fileMenu = new Menu("File");
        exit = new MenuItem("Exit");
        fileMenu.getItems().addAll(exit);
        menuBar= new MenuBar();
        menuBar.getMenus().addAll(fileMenu);
        this.setTop(menuBar);
        
    }
    
    public void createButtons(){
        EventHandler addAlbumHandler = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                addAlbumPopUp();
            }
        };
        EventHandler searchAlbumHandler = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                searchAlbumPopUp();
            }
        };
        
        
        
        anchorPane = new AnchorPane();
        buttonVBox = new VBox();
        this.setCenter(anchorPane);
        anchorPane.setStyle("-fx-background-color: #000000;");
        anchorPane.getChildren().add(buttonVBox);
        AnchorPane.setLeftAnchor(buttonVBox, windowWidth*0.4);
        AnchorPane.setTopAnchor(buttonVBox, windowHeight*0.2);
        
        Label media = new Label("Media");
        media.setFont(new Font("Arial", 28));
        media.setStyle("-fx-text-fill: red;");
        
                
        AnchorPane.setLeftAnchor(media, windowWidth*0.4);
        AnchorPane.setTopAnchor(media, windowHeight*0.1);
        anchorPane.getChildren().add(media);
        addAlbumButton = new Button("Add album");
        searchAlbumButton = new Button("Search for album");
        buttonVBox.getChildren().addAll(addAlbumButton,searchAlbumButton);
        buttonVBox.setSpacing(30);
        
        addAlbumButton.setOnAction(addAlbumHandler);
        searchAlbumButton.setOnAction(searchAlbumHandler);
    }
    
    public void addAlbumPopUp(){
        EventHandler addAlbumHandler = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                if(!albumTitleField.getText().isEmpty() && !albumYearField.getText().isEmpty()){
                        String selected = (String) albumGenreComboBox.getValue();
                        System.out.println(selected);
                        controller.handleAddAlbum(albumTitleField.getText(),(albumYearField.getText()),selected);
                    }
                    addAlbumStage.hide();
                }
            
        };
         EventHandler addArtistsHandler = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {
                if(!artistNameField.getText().isEmpty() && !artistAgeField.getText().isEmpty() && !artistNationalityField.getText().isEmpty()){
                        controller.handleAddArtist(artistNameField.getText(),(artistNationalityField.getText()),Integer.parseInt(artistAgeField.getText()));
                        artistNameField.clear();
                        artistNationalityField.clear();
                        artistAgeField.clear();
                        albumTitleField.setDisable(true);
                        albumYearField.setDisable(true);
                        albumGenreComboBox.setDisable(true);

                }
            }
        };
        addAlbumPopUp = new GridPane();
        addAlbumPopUp.setStyle("-fx-background-color: #000000;");
        addAlbumScreenLabel = new Label("Add an album");
        albumTitle =new Label("Title:");
        albumYear = new Label("Release year (YYYYMMDD):");
        albumGenre = new Label ("Genre:");
        artistLabel = new Label ("Add artist to album");
        artistName = new Label ("Artist Name");
        artistAge = new Label("Artist age");
        artistAgeField = new TextField();
        artistNameField = new TextField();
        artistNationality = new Label ("Artist nationality");
        artistNationalityField = new TextField();
        addArtistButton = new Button("Add");
        albumTitleField = new TextField();
        albumYearField = new TextField();
        albumGenreComboBox = new ComboBox();
        albumGenreComboBox.getItems().addAll("Rock","Pop","HipHop","RnB","House");
        albumGenreComboBox.setValue("Rock");
        doneButton = new Button("Done");
        addAlbumPopUp.add(addAlbumScreenLabel,1,0);
        addAlbumPopUp.add(albumTitle,0,1);
        addAlbumPopUp.add(albumTitleField,1,1);
        addAlbumPopUp.add(albumYear,0,2);
        addAlbumPopUp.add(albumYearField,1,2);
        addAlbumPopUp.add(albumGenre,0,3);
        addAlbumPopUp.add(albumGenreComboBox,1,3);
        addAlbumPopUp.add(artistLabel,1,4);
        addAlbumPopUp.add(artistName,0,5);
        addAlbumPopUp.add(artistNameField,1,5);
        addAlbumPopUp.add(artistNationality,0,6);
        addAlbumPopUp.add(artistNationalityField,1,6);
        addAlbumPopUp.add(addArtistButton,2,7);
        addAlbumPopUp.add(doneButton,0,8);
        addAlbumPopUp.add(artistAge,0,7);
        addAlbumPopUp.add(artistAgeField,1,7);
       
        
        addAlbumScreenLabel.setStyle("-fx-text-fill: white;");
        albumTitle.setStyle("-fx-text-fill: white;");
        artistAge.setStyle("-fx-text-fill: white;");
        albumYear.setStyle("-fx-text-fill: white;");
        albumGenre.setStyle("-fx-text-fill: white;");
        artistLabel.setStyle("-fx-text-fill: white;");
        artistName.setStyle("-fx-text-fill: white;");
        artistNationality.setStyle("-fx-text-fill: white;");
        
        addAlbumPopUp.setVgap(20);
        addAlbumPopUp.setHgap(20);
        
        doneButton.setOnAction(addAlbumHandler);
        addArtistButton.setOnAction(addArtistsHandler);
        
        Scene scene2 = new Scene(addAlbumPopUp,640,400);
        
        addAlbumStage = new Stage();
        addAlbumStage.setScene(scene2);
        addAlbumStage.initModality(Modality.APPLICATION_MODAL);
        addAlbumStage.showAndWait();
    }
      
    
    public void searchAlbumPopUp(){
        
        EventHandler searchAlbumHandler = new EventHandler<ActionEvent>(){

            @Override
            public void handle(ActionEvent event) {

                if( !searchAlbumTextField.getText().isEmpty()){
                    try{
                        String selected = (String) searchByComboBox.getValue();
                        switch (selected){
                            case "Title":
                                controller.handleSearchAlbumByTitle(searchAlbumTextField.getText());
                                break;
                            case "Artist":
                                controller.handleSearchAlbumByArtist(searchAlbumTextField.getText());
                                break;
                            case "Genre":
                                controller.handleSearchAlbumByGenre(searchAlbumTextField.getText());
                                break;
                            case "Rating":
                                controller.handleSearchAlbumByRating(Double.parseDouble(searchAlbumTextField.getText()));
                                break;
                        }
                        
                        addAlbumStage.hide();
                    } catch (SQLException ex) {
                      Logger.getLogger(ViewMain.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        };

        searchByLabel = new Label("Search By: ");
        searchButton = new Button("Search");
        searchByComboBox = new ComboBox();
        searchByComboBox.getItems().addAll("Title", "Artist", "Genre", "Rating");
        searchByComboBox.setValue("Title");
        searchAlbumTextField = new TextField();
        searchAlbumPopUp = new GridPane();
        searchAlbumPopUp.add(searchByLabel, 0, 0);
        searchAlbumPopUp.add(searchByComboBox, 1, 0);
        searchAlbumPopUp.add(searchAlbumTextField, 2, 0);
        searchAlbumPopUp.add(searchButton, 0, 1);
        searchAlbumPopUp.setPadding(new Insets(60, 12, 15, 12));
        searchAlbumPopUp.setVgap(20);
        searchAlbumPopUp.setHgap(20);
        Scene scene3 = new Scene(searchAlbumPopUp,500,200);
        searchAlbumPopUp.setStyle("-fx-background-color: #20B2AA;");
        searchButton.setOnAction(searchAlbumHandler);
        addAlbumStage = new Stage();
        addAlbumStage.setScene(scene3);
        addAlbumStage.initModality(Modality.APPLICATION_MODAL);
        addAlbumStage.showAndWait();
    }

    public void updateUI(ArrayList<Album> resultList) {
        
        ObservableList<Album> list = FXCollections.observableArrayList(resultList);
       
        VBox vBox = new VBox();
        
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10,0,0,10));
        
        Label label = new Label("Media");
        label.setFont(new Font("Arial", 20));
        
        tableView = new TableView<>();
        tableView.setEditable(false);
        TableColumn<Album,String> titleCol = new TableColumn("Title");
        TableColumn dateCol = new TableColumn("Release Date");
        TableColumn genreCol = new TableColumn("Genre");
        TableColumn idCol = new TableColumn("ID");
       
        TableColumn ratingCol = new TableColumn("Rating");
        ratingCol.setCellValueFactory(new PropertyValueFactory<>("reviews"));
           // tableView.getColumns().addAll(idCol, titleCol, dateCol, genreCol, ratingCol);
        
            EventHandler rateAlbumHandler = new EventHandler<ActionEvent>(){

                @Override
                public void handle(ActionEvent event) {
                    if( !rankField.getText().isEmpty() && !rankValueField.getText().isEmpty() ){
                         controller.handleRateAlbumEvent(Integer.parseInt(rankField.getText()), Double.parseDouble(rankValueField.getText()));
                    }
                }  
            };
            
             TableColumn artistCol = new TableColumn("Artists");
            artistCol.setCellValueFactory(new PropertyValueFactory<>("artist"));
            tableView.setMinWidth(600);
            tableView.getColumns().addAll(idCol, titleCol, dateCol, genreCol, artistCol, ratingCol);
            Label rankLabel = new Label("Rate album with id: ");
            Label rankValueLabel = new Label("Rating: ");
            Button rankButton = new Button("Rate!");
             rankField = new TextField();
             rankValueField = new TextField();
            HBox rankBox = new HBox();
            rankBox.setSpacing(5);
            rankBox.setPadding(new Insets(10,0,0,10));
            rankBox.getChildren().addAll(rankLabel, rankField, rankValueLabel, rankValueField);
            vBox.getChildren().addAll(label, tableView, rankBox, rankButton);
            rankButton.setOnAction(rateAlbumHandler);
        
        
        idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
        titleCol.setCellValueFactory(new PropertyValueFactory<>("title"));
        dateCol.setCellValueFactory(new PropertyValueFactory<>("year"));
        genreCol.setCellValueFactory(new PropertyValueFactory<>("genre"));
        ratingCol.setCellValueFactory(new PropertyValueFactory<>("reviews"));
        tableView.setItems(list);
   
        Scene tableScene = new Scene(new Group());
        Stage tableStage = new Stage();
        tableStage.setWidth(600);
        tableStage.setHeight(600);
        
        ((Group) tableScene.getRoot()).getChildren().addAll(vBox);
        tableStage.setScene(tableScene);
        tableStage.initModality(Modality.APPLICATION_MODAL);
        tableStage.showAndWait();  
    }
}
