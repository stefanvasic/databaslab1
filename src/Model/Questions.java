/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author stefanvasic
 */
public interface Questions {
    public void addAlbum(String title, String year, String genre) throws SQLException;
    public ArrayList<Album> addReview(int id,double rating);
    public ArrayList<Album> searchByTitle(String title);
    public ArrayList<Album> searchByArtist(String artist);
    public ArrayList<Album> searchByGenre(String genre);
    public ArrayList<Album> searchByRating(Double rating);
    public void addArtist(String name, String nationality, int age);    
}
