/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class Artist extends Person{
    
    public Artist(int id, String name, String nationality,int age){
        super(id, name, nationality,age);
    }
    
    @Override
    public String toString (){
        String info = super.getName();
        return info;
    }
}
