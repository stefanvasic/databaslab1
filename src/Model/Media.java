/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class Media {
    private int id;
    private String genre, title, year;
    //private Review review;
    public Media(int id, String title, String year, String genre){
        this.id=id;
        this.genre=genre;
        this.title=title;
        this.year=year;
    }

    public int getId() {
        return id;
    }

    public String getGenre() {
        return genre;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    
    
    public String toString(){
        String info = title;
        return info;
    }
    
}
