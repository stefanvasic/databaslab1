/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class Movie extends Media{
    private ArrayList<Director> directors;
    
    
    public Movie(int id, String title, String year, String genre){
        super(id, title,year,genre);
    }
}
