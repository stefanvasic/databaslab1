/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.bson.types.ObjectId;

/**
 *
 * @author stefanvasic
 */
public class MongoDbQueries implements Questions{
    private MongoDatabase db;
    private ArrayList<Album> albums;
    private ArrayList<Artist> artists;
    private ArrayList<ObjectId> id;
    private Block block;
    
    
    public MongoDbQueries(MongoDatabase db){
        this.db=db;
        artists = new ArrayList();
        id = new ArrayList();
        albums = new ArrayList();
    }
    
    
    @Override
    public void addAlbum(String title, String year, String genre) throws SQLException {
        for(int i=0;i<artists.size();i++){
            BasicDBObject addArtist = new BasicDBObject();
            List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
            obj.add(new BasicDBObject("Name", artists.get(i).getName()));
            obj.add(new BasicDBObject("Nationality", artists.get(i).getNationality()));
            obj.add(new BasicDBObject("Age", artists.get(i).getAge()));
            addArtist.put("$and", obj);
            FindIterable<Document> iterable = db.getCollection("artist").find(addArtist);
            
            if(iterable.first()==null){
                MongoCollection<Document> collection = db.getCollection("artist");
                Document document = new Document();
                document.put("Name", artists.get(i).getName());
                document.put("Nationality", artists.get(i).getNationality());
                document.put("Age", artists.get(i).getAge());
                collection.insertOne(document);
                ObjectId tmp = (ObjectId)document.get( "_id" );
                id.add(tmp);
               
            }
            else{
                 block = new Block<Document>() {
                    @Override
                    public void apply(Document t) {
                        id.add(t.getObjectId("_id"));
                    }
                };
                iterable.forEach(block);
            }
        }
        
        
        BasicDBObject checkExsistingAlbum = new BasicDBObject();
	List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
	obj.add(new BasicDBObject("Title", title));
	obj.add(new BasicDBObject("ReleaseDate", year));
        obj.add(new BasicDBObject("Genre", genre));
	checkExsistingAlbum.put("$and", obj);
        FindIterable<Document> iterable = db.getCollection("album").find(checkExsistingAlbum);

        if(iterable.first()==null){
            System.out.println("finns inget album");
            MongoCollection<Document> collection = db.getCollection("album");
            Document document1 = new Document();
            document1.put("Title", title);
            document1.put("ReleaseDate", year);
            document1.put("Genre", genre);
            document1.put("Artist", id);
            collection.insertOne(document1);
        }
        else{
            System.out.println("Album finns redan");
        }
        artists.clear();
        id.clear();
        
        
    }

    @Override
    public ArrayList<Album> addReview(int id, double rating) {
        System.out.println("add review");
        if(id>0 && id<=albums.size()){
            String tmpTitle = albums.get(id-1).getTitle();
            String tmpReleaseDate = albums.get(id-1).getYear();
            String tmpGenre = albums.get(id-1).getGenre();
            BasicDBObject checkIfAlbumIsRated = new BasicDBObject();
            List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
            obj.add(new BasicDBObject("Title", tmpTitle));
            obj.add(new BasicDBObject("ReleaseDate", tmpReleaseDate));
            obj.add(new BasicDBObject("Genre", tmpGenre));
            checkIfAlbumIsRated.put("$and", obj);
            FindIterable<Document> iterable2 = db.getCollection("album").find(checkIfAlbumIsRated);
            block = new Block<Document>() {
                        @Override
                        public void apply(Document t) {
                            if(t.size()>5){
                                System.out.println("Rating exist");
                            }
                            else{
                                 db.getCollection("album").updateOne(new BasicDBObject("_id", t.getObjectId("_id")),
                                                                 new BasicDBObject("$set", new BasicDBObject("Rating", rating)));
                             albums.get(id-1).addReview(new Review(rating));
                            }
                        }
                    };
            iterable2.forEach(block);
        
        }return albums;
    }

    @Override
    public ArrayList<Album> searchByTitle(String title) {
        System.out.println("search by title");
         albums = new ArrayList<>();
        FindIterable<Document> iterable = db.getCollection("album").find(new Document("Title", new Document("$regex", title)));
         block = new Block<Document>() {
       
            @Override
            public void apply(Document t) {
                albums.add(new Album(albums.size()+1,t.getString("Title"), t.getString("ReleaseDate"), t.getString("Genre")));
                if(t.size()>5){
                            albums.get(albums.size()-1).addReview(new Review(t.getDouble("Rating")));
                        }
                Object o = t.get("Artist");
                String string[] = o.toString().split(",");
                for(int i=1;i<string.length;i++){
                    string[i]=string[i].trim();
                }
                string[0] = string[0].replace("[", "");
                string[string.length-1] = string[string.length-1].replace("]", "");
                for(int i=0;i<string.length;i++){
                    Document query = new Document();
                    query.put("_id", new ObjectId(string[i]));
                    FindIterable<Document> findArtist = db.getCollection("artist").find(query);
                    Block<Document> block2 = new Block<Document>() {
                        
                        @Override
                        public void apply(Document t) {
                            albums.get(albums.size()-1).addArtist(new Artist(0,t.getString("Name"),t.getString("Nationality"),t.getInteger("Age")));
                            
                        }
                    };
                        
                    
                    findArtist.forEach(block2);
                }

            }
        };
        
        iterable.forEach(block);
       
        
      
        return albums;
    }

    @Override
    public ArrayList<Album> searchByArtist(String artistName) {
        System.out.println("search by artist");
         artists = new ArrayList<>();
         albums = new ArrayList<>();
        FindIterable<Document> iterable = db.getCollection("artist").find(new Document("Name", new Document("$regex", artistName)));
         block = new Block<Document>() {
         
            @Override
            public void apply(Document t) {
                id.add(t.getObjectId("_id"));
                System.out.println("["+t.getObjectId("_id")+"]");
            }
            
            
            };
            iterable.forEach(block);
            
            for(int i=0;i<id.size();i++){
                System.out.println("id: " + id.get(i).toString());
                 Document query = new Document();
                    query.put("Artist", new ObjectId(id.get(i).toString()));
                FindIterable<Document> iterable1 = db.getCollection("album").find( query);                 //new Document("Artist", new Document("$in", id.get(i))));       //new Document("Artist:","ObjectId("+ id.get(i)+")"));      //"{ field: { $in: [<"+id.get(i)+ " ] } }"));  //new Document("$in:", "["+id.get(i).toString()+"]")));
                  block = new Block<Document>() {
         
            @Override
            public void apply(Document t) {
                albums.add(new Album(albums.size()+1,t.getString("Title"), t.getString("ReleaseDate"), t.getString("Genre")));
                if(t.size()>5){
                            albums.get(albums.size()-1).addReview(new Review(t.getDouble("Rating")));
                        }
                Object o = t.get("Artist");
                String string[] = o.toString().split(",");
                for(int i=1;i<string.length;i++){
                    string[i]=string[i].trim();
                }
                string[0] = string[0].replace("[", "");
                string[string.length-1] = string[string.length-1].replace("]", "");
                for(int i=0;i<string.length;i++){
                    Document query = new Document();
                    query.put("_id", new ObjectId(string[i]));
                    FindIterable<Document> findArtist = db.getCollection("artist").find(query);
                    Block<Document> block2 = new Block<Document>() {
                        
                        @Override
                        public void apply(Document t) {
                            albums.get(albums.size()-1).addArtist(new Artist(0,t.getString("Name"),t.getString("Nationality"),t.getInteger("Age")));
                            
                        }
                    };
                        
                    
                    findArtist.forEach(block2);
                }
            }
            
            
            };
            iterable1.forEach(block);
            }
            
       
        
        id.clear();
        artists.clear();
        return albums;
    }

    @Override
    public ArrayList<Album> searchByGenre(String genre) {
        System.out.println("search by genre");
       albums = new ArrayList<>();
        FindIterable<Document> iterable = db.getCollection("album").find(new Document("Genre", new Document("$regex", genre)));
         block = new Block<Document>() {
       
            @Override
            public void apply(Document t) {
                albums.add(new Album(albums.size()+1,t.getString("Title"), t.getString("ReleaseDate"), t.getString("Genre")));
                if(t.size()>5){
                            albums.get(albums.size()-1).addReview(new Review(t.getDouble("Rating")));
                        }
                Object o = t.get("Artist");
                String string[] = o.toString().split(",");
                for(int i=1;i<string.length;i++){
                    string[i]=string[i].trim();
                }
                string[0] = string[0].replace("[", "");
                string[string.length-1] = string[string.length-1].replace("]", "");
                for(int i=0;i<string.length;i++){
                    Document query = new Document();
                    query.put("_id", new ObjectId(string[i]));
                    FindIterable<Document> findArtist = db.getCollection("artist").find(query);
                    Block<Document> block2 = new Block<Document>() {
                        
                        @Override
                        public void apply(Document t) {
                            albums.get(albums.size()-1).addArtist(new Artist(0,t.getString("Name"),t.getString("Nationality"),t.getInteger("Age")));
                            
                        }
                    };
                        
                    
                    findArtist.forEach(block2);
                }

            }
        };
        
        iterable.forEach(block);
       
        
      
        return albums;
    }

    @Override
    public ArrayList<Album> searchByRating(Double rating) {
        System.out.println("search by rating");
        albums = new ArrayList<>();
        BasicDBObject query = new BasicDBObject("Rating", new BasicDBObject(
                "$exists", true).append("$gt", rating));
        FindIterable<Document> iterable = db.getCollection("album").find(query);
         block = new Block<Document>() {
       
            @Override
            public void apply(Document t) {
                albums.add(new Album(albums.size()+1,t.getString("Title"), t.getString("ReleaseDate"), t.getString("Genre")));
                albums.get(albums.size()-1).addReview(new Review(t.getDouble("Rating")));
                Object o = t.get("Artist");
                String string[] = o.toString().split(",");
                for(int i=1;i<string.length;i++){
                    string[i]=string[i].trim();
                }
                string[0] = string[0].replace("[", "");
                string[string.length-1] = string[string.length-1].replace("]", "");
                for(int i=0;i<string.length;i++){
                    Document query = new Document();
                    query.put("_id", new ObjectId(string[i]));
                    FindIterable<Document> findArtist = db.getCollection("artist").find(query);
                    Block<Document> block2 = new Block<Document>() {
                        
                        @Override
                        public void apply(Document t) {
                            albums.get(albums.size()-1).addArtist(new Artist(0,t.getString("Name"),t.getString("Nationality"),t.getInteger("Age")));
                            
                        }
                    };
                        
                    
                    findArtist.forEach(block2);
                }

            }
        };
        
        iterable.forEach(block);
        return albums;
    }

    @Override
    public void addArtist(String name, String nationality, int age) {
        artists.add(new Artist(0,name,nationality,age));
    }
}
