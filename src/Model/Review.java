/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class Review {
    private double rating;
    
    public Review(Double rating){
        this.rating=rating;
    }

    public double getRating() {
        return rating;
    }

    @Override
    public String toString() {
        String info = ""+rating;
        return info;
    }

}
