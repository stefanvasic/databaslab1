/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class Person {
    private int id,age;

    private String name, nationality;
    
    public Person(int id,String name, String nationality, int age){
        this.id=id;
        this.name=name;
        this.nationality=nationality;
        this.age=age;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getNationality() {
        return nationality;
    }
    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", name=" + name + ", nationality=" + nationality + '}';
    }
    
}
