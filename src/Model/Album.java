/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class Album extends Media{
    private ArrayList<Artist> artist;
    private ArrayList<Review> reviews;
    public Album(int id, String title, String year, String genre) {
        super(id, title, year, genre);
        artist = new ArrayList<>();
        reviews = new ArrayList<>();
    }
    
    public ArrayList<Artist> getArtist() {
        return artist;
    }

    public ArrayList<Review> getReviews() {
        return reviews;
    }
    
    /**
     * add review
     * @param review 
     */
    public void addReview(Review review) {
        this.reviews.add(review);
    }
   /**
    * add artist
    * @param artist 
    */
    public void addArtist(Artist artist){
        this.artist.add(artist);
    }
     public String toString(){
        String info = "Id: " + super.getId() + "Title: " + super.getTitle() + "year:" + super.getYear() + "genre: " + super.getGenre() + "artist: " + artist.toString();
        return info;
    }
    
}

