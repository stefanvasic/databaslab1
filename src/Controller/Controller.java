/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.Album;
import Model.Questions;
import View.ViewMain;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Manjinder Singh & Stefan Vasic TIDAA2
 */
public class Controller {
    private Questions model;
    private ViewMain viewMain;
    private ArrayList<Album> result;
    
    public Controller(ViewMain viewMain, Questions model){
        this.viewMain = viewMain;
        this.model = model;
        result = new ArrayList<>();
    }
    
    public void handleAddAlbum (String title, String year, String genre) {
        new Thread() {
            @Override
            public void run() {
                try {
                    model.addAlbum(title, year, genre);
                } catch (SQLException ex) {
                    Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
                }
                 
             }
         }.start();
    }
    
    public void handleAddArtist (String name, String nationality,int age){
        new Thread() {
            @Override
            public void run() {
                 model.addArtist(name, nationality, age);
             }
         }.start();
    }
    
    
    public void handleSearchAlbumByTitle(String title) throws SQLException{
        new Thread() {
            @Override
            public void run() {
                result = model.searchByTitle(title);
                 javafx.application.Platform.runLater(
                 new Runnable(){
                     public void run(){
                         viewMain.updateUI(result);
                     }
                 });
            }
        }.start();
    }
  
    public void handleSearchAlbumByArtist(String artist){
        new Thread() {
            @Override
            public void run() {
                result = model.searchByArtist(artist);
                 javafx.application.Platform.runLater(
                 new Runnable(){
                     public void run(){
                         viewMain.updateUI(result);
                     }
                 });
            }
        }.start();
    }
    
    public void handleSearchAlbumByGenre(String genre){
        new Thread() {
            @Override
            public void run() {
                result = model.searchByGenre(genre);
                 javafx.application.Platform.runLater(
                 new Runnable(){
                     public void run(){
                         viewMain.updateUI(result);
                     }
                 });
            }
        }.start();
    }
    public void handleSearchAlbumByRating(double rating){
        new Thread() {
            @Override
            public void run() {
                result = model.searchByRating(rating);
                 javafx.application.Platform.runLater(
                 new Runnable(){
                     public void run(){
                         viewMain.updateUI(result);
                     }
                 });
            }
        }.start();
    }
    
    public void handleRateAlbumEvent(int id, double rating){
        new Thread() {
            @Override
            public void run() {
                 result = model.addReview(id, rating);
                 javafx.application.Platform.runLater(
                 new Runnable(){
                     public void run(){
                         viewMain.updateUI(result);
                     }
                 });
            }
        }.start();
    }
}

